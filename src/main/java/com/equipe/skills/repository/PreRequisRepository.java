package com.equipe.skills.repository;

import com.equipe.skills.entity.Competence;
import com.equipe.skills.entity.PreRequis;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface PreRequisRepository extends JpaRepository<PreRequis, Long> {
        @Query("select p from PreRequis p left join fetch p.competence c where c.id = ?1")
        public List<PreRequis> findByIdWithPrerequisites(Long idCompetence);
}