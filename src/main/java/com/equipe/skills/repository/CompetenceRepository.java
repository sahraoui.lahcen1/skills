package com.equipe.skills.repository;

import com.equipe.skills.entity.Competence;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface CompetenceRepository extends JpaRepository<Competence, Long> {

}