package com.equipe.skills.exception;


import lombok.Data;

import java.time.LocalDateTime;

/**
 * Permet de retourner une réponse en cas d'erreur 404
 */
@Data
public class NotFoundResponse {
     private String status = "Not found";
    private LocalDateTime timestamp = LocalDateTime.now();
    private String message;

    public NotFoundResponse(String message) {
        this.message = message;
    }

}
