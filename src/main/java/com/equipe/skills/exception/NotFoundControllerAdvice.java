package com.equipe.skills.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Gestion des exceptions :Controller Advice permettant la gestion des exceptions NotFoundexception générés pas l'API.
 */
@ControllerAdvice
public class NotFoundControllerAdvice {
    private static final Logger log = LoggerFactory.getLogger(NotFoundControllerAdvice.class);

    /**
     * Méthode appelée quand une exception de type NoFoundException est levé, mais pas géré par le code.
     *
     * @param exception instance de l'exception levée
     */
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = {NotFoundException.class})
    public ResponseEntity<NotFoundResponse> handleException(RuntimeException exception) {
        log.warn(exception.getMessage());
        return new ResponseEntity<>(new NotFoundResponse(exception.getMessage()), HttpStatus.NOT_FOUND);
    }

}
