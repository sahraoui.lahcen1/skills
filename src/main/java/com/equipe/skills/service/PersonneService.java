package com.equipe.skills.service;

import com.equipe.skills.entity.Personne;
import com.equipe.skills.exception.NotFoundException;
import com.equipe.skills.repository.PersonneRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonneService {

    private PersonneRepository personneRepository;

    public PersonneService(PersonneRepository personneRepository) {
        this.personneRepository = personneRepository;
    }

    /**
     * Récupère toutes les personnes
     *
     * @return List<Personne>
     */
    public List<Personne> getAllPersonnes() {
        return personneRepository.findAll();
    }

    /**
     * Crée une personne
     *
     * @param personne
     * @return personne
     */
    public Personne create(Personne personne) {
        return personneRepository.save(personne);
    }

    /**
     * Récupère une personne par son id
     *
     * @param id
     * @return
     */
    public Personne getPersonneById(Long id) {
        Personne personne = personneRepository.findById(id).orElseThrow(() -> new NotFoundException("Personne non trouvée"));
        return personne;
    }

    /**
     * Supprime une personne par son id
     *
     * @param id
     */
    public void deleteById(Long id) {
        personneRepository.deleteById(id);
    }
}
