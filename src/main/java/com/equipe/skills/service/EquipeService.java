package com.equipe.skills.service;

import com.equipe.skills.entity.Equipe;
import com.equipe.skills.entity.Personne;
import com.equipe.skills.exception.NotFoundException;
import com.equipe.skills.repository.EquipeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EquipeService {

    private EquipeRepository equipeRepository;
    private PersonneService personneService;


    public EquipeService(EquipeRepository equipeRepository, PersonneService personneService) {
        this.equipeRepository = equipeRepository;
        this.personneService = personneService;
    }

    public List<Equipe> getEquipes() {
        return equipeRepository.findAll();
    }

    public Equipe create(Equipe equipe) {
        return equipeRepository.save(equipe);
    }

    public Equipe getEquipeById(Long id) {
        Equipe equipe = equipeRepository.findById(id).orElseThrow(() -> new NotFoundException("Equipe non trouvée"));
        return equipe;
    }

    public void deleteById(Long id) {
        equipeRepository.deleteById(id);
    }

    public Equipe addMembre(Equipe equipe, Personne personne) {
        personne.setEquipe(equipe);
        personneService.create(personne);
        equipe.getMembres().add(personne);
        return equipe;
    }
}
