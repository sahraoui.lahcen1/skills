package com.equipe.skills.service;

import com.equipe.skills.entity.PreRequis;
import com.equipe.skills.exception.NotFoundException;
import com.equipe.skills.repository.PreRequisRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PreRequisService {

    private PreRequisRepository preRequisRepository;

    public PreRequisService(PreRequisRepository preRequisRepository) {
        this.preRequisRepository = preRequisRepository;
    }


    public List<PreRequis> getAllPreRequisList() {
        return preRequisRepository.findAll();
    }

    public PreRequis create(PreRequis personne) {
        return preRequisRepository.save(personne);
    }


    public PreRequis getPreRequisById(Long id) {
        PreRequis personne = preRequisRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("PreRequis non trouvée"));
        return personne;
    }

    public void deleteById(Long id) {
        preRequisRepository.deleteById(id);
    }
}
