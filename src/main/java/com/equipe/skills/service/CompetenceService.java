package com.equipe.skills.service;

import com.equipe.skills.dto.CompetencePreRequisDto;
import com.equipe.skills.dto.PreRequisDto;
import com.equipe.skills.entity.Competence;
import com.equipe.skills.entity.PreRequis;
import com.equipe.skills.exception.NotFoundException;
import com.equipe.skills.repository.CompetenceRepository;
import com.equipe.skills.repository.PreRequisRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompetenceService {
    private CompetenceRepository competenceRepository;
    private PreRequisRepository preRequisRepository;

    public CompetenceService(CompetenceRepository competenceRepository, PreRequisRepository preRequisRepository) {
        this.competenceRepository = competenceRepository;
        this.preRequisRepository = preRequisRepository;
    }

    public List<Competence> getCompetences() {
        return competenceRepository.findAll();
    }

    public Competence getCompetenceById(Long id) {
        Competence competence = competenceRepository.findById(id).orElseThrow(() -> new NotFoundException("Competence non trouvée"));
        return competence;
    }

    public Competence create(Competence competence) {
        return competenceRepository.save(competence);
    }

    public void deleteById(Long id) {
        competenceRepository.deleteById(id);
    }

    public CompetencePreRequisDto getCompetenceWithPrerequisites(Long id) {
        Competence competence = getCompetenceById(id);
        List<PreRequis> preRequisList = preRequisRepository.findByIdWithPrerequisites(competence.getId());

        CompetencePreRequisDto competencePreRequisDto = new CompetencePreRequisDto(competence, PreRequisDto.getList(preRequisList));
        return competencePreRequisDto;
    }
}
