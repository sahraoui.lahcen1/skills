package com.equipe.skills.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Entity
@NoArgsConstructor
@ToString
@Table(name = "competence_personne")
public class CompetencePersonne {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private Personne personne;
    @ManyToOne
    private Competence competence;
    @Enumerated(EnumType.STRING)
    private NiveauCompetence niveau;
}