package com.equipe.skills.entity;

public enum NiveauCompetence {
    AUCUNE_CONNAISSANCE(0),
    CONNAIT_NOM_COMPETENCE(1),
    CONNAIT_NOM_DESCRIPTION_COMPETENCE(2),
    CONNAIT_NOM_DESCRIPTION_FONCTION_COMPETENCE(3),
    CONNAIT_NOM_DESCRIPTION_FONCTION_FONCTIONNEMENT_COMPETENCE(4),
    UTILISATION_BASIQUE(5),
    UTILISATION_INTERMEDIAIRE(6),
    UTILISATION_AVANCEE(7),
    UTILISATION_EXPERTE(8),
    MAITRISE_COMPETENCE(9),
    ENSEIGNEMENT_COMPETENCE(10);

    private final int niveau;

    NiveauCompetence(int niveau) {
        this.niveau = niveau;
    }

    public int getNiveau() {
        return niveau;
    }
}

