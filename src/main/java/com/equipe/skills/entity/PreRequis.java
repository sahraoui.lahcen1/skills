package com.equipe.skills.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@ToString
@Entity
@Table(name = "pre_requis")
public class PreRequis implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private Competence competence;

    private NiveauCompetence niveau;
    @ManyToOne
    private Competence competenceRequis;

    private NiveauCompetence niveauRequis;
    @Enumerated(EnumType.STRING)
    private TypeValidation typeValidation;
}