package com.equipe.skills;

import com.equipe.skills.entity.*;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Component
public class InitDb implements CommandLineRunner {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    public void run(String... args) throws Exception {
        System.out.println(" ApplicationRunner called");
        Equipe equipe = new Equipe();
        equipe.setNom("Equipe 1");
        entityManager.persist(equipe);

        Personne personne = new Personne();
        personne.setNom("Personne 1");
        personne.setPrenom("Prenom 1");
        personne.setEquipe(equipe);
        entityManager.persist(personne);

        Personne personne2 = new Personne();
        personne2.setNom("Personne 2");
        personne2.setPrenom("Prenom 2");
        personne2.setEquipe(equipe);
        entityManager.persist(personne2);

        Personne personne3 = new Personne();
        personne3.setNom("Personne 3");
        personne3.setPrenom("Prenom 3");
        entityManager.persist(personne3);

        Competence competence = new Competence();
        competence.setNom("Java");
        competence.setDescription("Java");
        entityManager.persist(competence);

        Competence competence2 = new Competence();
        competence2.setNom("Spring");
        competence2.setDescription("Spring");
        entityManager.persist(competence2);

        Competence competence3 = new Competence();
        competence3.setNom("Hibernate");
        competence3.setDescription("Hibernate");
        entityManager.persist(competence3);

        Competence competence4 = new Competence();
        competence4.setNom("Angular");
        competence4.setDescription("Angular");
        entityManager.persist(competence4);

        Competence competence5 = new Competence();
        competence5.setNom("React");
        competence5.setDescription("React");
        entityManager.persist(competence5);

        Competence competence6 = new Competence();
        competence6.setNom("Vue");
        competence6.setDescription("Vue");
        entityManager.persist(competence6);

        Competence competence7 = new Competence();
        competence7.setNom("Node");
        competence7.setDescription("Node");
        entityManager.persist(competence7);

        PreRequis prerequis = new PreRequis();
        prerequis.setCompetence(competence2);
        prerequis.setCompetenceRequis(competence);
        prerequis.setNiveau(NiveauCompetence.UTILISATION_INTERMEDIAIRE);
        prerequis.setNiveauRequis(NiveauCompetence.UTILISATION_BASIQUE);

        entityManager.persist(prerequis);

        PreRequis prerequis1 = new PreRequis();
        prerequis1.setCompetence(competence2);
        prerequis1.setCompetenceRequis(competence3);
        prerequis1.setNiveau(NiveauCompetence.UTILISATION_INTERMEDIAIRE);
        prerequis1.setNiveauRequis(NiveauCompetence.UTILISATION_BASIQUE);

        entityManager.persist(prerequis1);

        CompetencePersonne competencePersonne = new CompetencePersonne();
        competencePersonne.setCompetence(competence);
        competencePersonne.setPersonne(personne);
        competencePersonne.setNiveau(NiveauCompetence.UTILISATION_BASIQUE);
        entityManager.persist(competencePersonne);

        CompetencePersonne competencePersonne2 = new CompetencePersonne();
        competencePersonne2.setCompetence(competence2);
        competencePersonne2.setPersonne(personne);
        competencePersonne2.setNiveau(NiveauCompetence.UTILISATION_INTERMEDIAIRE);
        entityManager.persist(competencePersonne2);

        CompetencePersonne competencePersonne3 = new CompetencePersonne();
        competencePersonne3.setCompetence(competence3);
        competencePersonne3.setPersonne(personne);
        competencePersonne3.setNiveau(NiveauCompetence.UTILISATION_AVANCEE);
        entityManager.persist(competencePersonne3);

        entityManager.flush();

    }

}
