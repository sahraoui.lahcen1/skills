package com.equipe.skills.controller;

import com.equipe.skills.entity.Personne;
import com.equipe.skills.service.PersonneService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/personnes")
public class PersonneController {
    private PersonneService personneService;

    public PersonneController(PersonneService personneService) {
        this.personneService = personneService;
    }

    @GetMapping
    public ResponseEntity<List<Personne>> getAllPersonnes() {
        return ResponseEntity.ok(personneService.getAllPersonnes());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Personne> getPersonneById(@PathVariable Long id) {
        return ResponseEntity.ok(personneService.getPersonneById(id));
    }

    @PostMapping
    public ResponseEntity<Personne> createPersonne(Personne personne) {
        return ResponseEntity.ok(personneService.create(personne));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deletePersonneById(@PathVariable Long id) {
        personneService.deleteById(id);
        return ResponseEntity.ok("Personne supprimée");
    }
}
