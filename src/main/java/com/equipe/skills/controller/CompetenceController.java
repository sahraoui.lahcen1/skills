package com.equipe.skills.controller;

import com.equipe.skills.dto.CompetencePreRequisDto;
import com.equipe.skills.entity.Competence;
import com.equipe.skills.service.CompetenceService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/competences")
public class CompetenceController {

    private CompetenceService competenceService;

    public CompetenceController(CompetenceService competenceService) {
        this.competenceService = competenceService;
    }

    @GetMapping
    public ResponseEntity<List<Competence>> getCompetences() {
        return ResponseEntity.ok(competenceService.getCompetences());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Competence> getCompetenceById(@PathVariable Long id) {
        return ResponseEntity.ok(competenceService.getCompetenceById(id));
    }

    @PostMapping
    public ResponseEntity<Competence> createCompetence(@RequestBody Competence competence) {
        return new ResponseEntity<>(competenceService.create(competence), HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCompetenceById(@PathVariable Long id) {
        competenceService.deleteById(id);
        return ResponseEntity.ok("Competence supprimée");
    }

    // getComptence with prerequisites
    @GetMapping("/{id}/prerequisites")
    public ResponseEntity<CompetencePreRequisDto> getCompetenceWithPrerequisites(@PathVariable Long id) {
        return ResponseEntity.ok(competenceService.getCompetenceWithPrerequisites(id));
    }
}
