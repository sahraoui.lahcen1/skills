package com.equipe.skills.controller;

import com.equipe.skills.entity.PreRequis;
import com.equipe.skills.service.PreRequisService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/prerequis")
public class PreRequisController {

    private PreRequisService preRequisService;

    public PreRequisController(PreRequisService preRequisService) {
        this.preRequisService = preRequisService;
    }

    @GetMapping
    public ResponseEntity<List<PreRequis>> getAllPreRequis() {
        return ResponseEntity.ok(preRequisService.getAllPreRequisList());
    }
    @GetMapping("/{id}")
    public ResponseEntity<PreRequis> getPreRequisById(@PathVariable Long id) {
        return ResponseEntity.ok(preRequisService.getPreRequisById(id));
    }
}
