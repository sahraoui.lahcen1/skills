package com.equipe.skills.controller;

import com.equipe.skills.entity.Equipe;
import com.equipe.skills.entity.Personne;
import com.equipe.skills.service.EquipeService;
import com.equipe.skills.service.PersonneService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/equipes")
public class EquipeController {
    private EquipeService equipeService;
    private PersonneService personneService;

    public EquipeController(EquipeService equipeService, PersonneService personneService) {
        this.equipeService = equipeService;
        this.personneService = personneService;
    }

    @GetMapping
    public ResponseEntity<List<Equipe>> getEquipes() {
        return ResponseEntity.ok(equipeService.getEquipes());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Equipe> getEquipeById(@PathVariable Long id) {
        return ResponseEntity.ok(equipeService.getEquipeById(id));
    }

    @PostMapping
    public ResponseEntity<Equipe> createEquipe(@RequestBody Equipe equipe) {
        return new ResponseEntity<>(equipeService.create(equipe), HttpStatus.CREATED);
    }

    @PutMapping("/{equipe}/membres/{personne}")
    public Equipe addMembre(@PathVariable Equipe equipe, @PathVariable Personne personne) {
        return equipeService.addMembre(equipe, personne);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteEquipeById(@PathVariable Long id) {
        equipeService.deleteById(id);
        return ResponseEntity.ok("Equipe supprimée");
    }


}
