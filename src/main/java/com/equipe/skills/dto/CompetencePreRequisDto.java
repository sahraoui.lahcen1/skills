package com.equipe.skills.dto;

import com.equipe.skills.entity.Competence;
import com.equipe.skills.entity.PreRequis;
import lombok.Data;

import java.util.List;

@Data
public class CompetencePreRequisDto {
    private Competence competence;
    private List<PreRequisDto> preRequis;

    public CompetencePreRequisDto(Competence competence, List<PreRequisDto> preRequis) {
        this.competence = competence;
        this.preRequis = preRequis;
    }
}
