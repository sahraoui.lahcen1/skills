package com.equipe.skills.dto;

import com.equipe.skills.entity.Competence;
import com.equipe.skills.entity.NiveauCompetence;
import com.equipe.skills.entity.PreRequis;
import com.equipe.skills.entity.TypeValidation;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Data
@Builder
public class PreRequisDto {

    private Long id;
    private NiveauCompetence niveau;
    private Competence competenceRequis;
    private NiveauCompetence niveauRequis;
    private TypeValidation typeValidation;


    // from PreRequis to PreRequisDto
    public static PreRequisDto toPreRequisDto(PreRequis preRequis) {
        return PreRequisDto.builder().
                id(preRequis.getId())
                .niveau(preRequis.getNiveau())
                .competenceRequis(preRequis.getCompetenceRequis())
                .niveauRequis(preRequis.getNiveauRequis())
                .typeValidation(preRequis.getTypeValidation()).build();
    }

    // from List<PreRequis> to List<PreRequisDto> using stream
    public static List<PreRequisDto> getList(List<PreRequis> preRequisList) {
        return preRequisList.stream().map(PreRequisDto::toPreRequisDto).collect(Collectors.toList());
    }
}


